package com.mars.springboot.shop.base;

/**
 * 通用的响应状态码 枚举类
 * Created by Administrator on 2019/10/26.
 */

public enum StatusCode {
//    Success(0,"成功"),
//    Fail(1,"失败"),
//    Init(2,"未校验"),
//    IsErrorUserOrPassword(999,"账号或密码有误!"),
//    InvalidParams(201,"非法的参数!"),
//    UserNotLogin(202,"用户没登录"),
//    UnknownError(500,"未知异常，请联系管理员!"),
//    InvalidCode(501,"验证码不正确!"),
//    AccountPasswordNotMatch(502,"账号密码不匹配!"),
//    AccountHasBeenLocked(503,"账号已被锁定,请联系管理员!"),
//    AccountValidateFail(504,"账户验证失败!"),
//    IsBlankFail(110,"输入信息不合法，存在信息为空！")

    OK(200,"成功"),
    CREATED(201,"创建成功"),
    DELETED(204,"删除成功"),
    BADREQUEST(400,"请求的地址不存在或者包含不支持的参数"),
    UNAUTHORIZED(401,"未授权"),
    FORBIDDEN(403,"被禁止访问"),
    NOTFOUND(404,"请求的资源不存在"),
    UnprocesableEntity(422,"[POST/PUT/PATCH] 当创建一个对象时，发生一个验证错误"),
    INTERNALERROR(500,"内部错误"),
    Fail(110,"失败"),
    IsBlankFail(111,"输入信息不合法，存在信息为空！"),
    Init(112,"未校验"),
    InserError(999,"数据插入失败！"),
    IsErrorUserOrPassword(206,"账号或密码有误!"),
    InvalidParams(203,"非法的参数!"),
    UserNotLogin(205,"用户没登录"),
    UnknownError(520,"未知异常，请联系管理员!"),
    InvalidCode(501,"验证码不正确!"),
    AccountPasswordNotMatch(502,"账号密码不匹配!"),
    AccountHasBeenLocked(503,"账号已被锁定,请联系管理员!"),
    AccountValidateFail(504,"账户验证失败!"),
    AccountValidateError(505,"用户名已被占用，请重新填入!"),
    selectIsBank(555,"查询结果为空"),
    ;

    private Integer code;
    private String msg;

    StatusCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
