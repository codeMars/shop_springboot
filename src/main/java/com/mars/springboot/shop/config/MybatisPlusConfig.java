package com.mars.springboot.shop.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(value = {PaginationInterceptor.class})
@MapperScan(basePackages = {"com.mars.springboot.shop.dao","com.mars.springboot.shop.dto"})

public class MybatisPlusConfig {
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();

        //设置请求页码大于最大页后，true为回调为首页，false继续操作，默认false
        paginationInterceptor.setOverflow(false);
        //设置最大单页限制数量，默认500条，-1不受限制
        paginationInterceptor.setLimit(500);

        return paginationInterceptor;
    }
}
