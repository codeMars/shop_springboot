package com.mars.springboot.shop.controller;


import com.mars.springboot.shop.base.StatusCode;
import com.mars.springboot.shop.dto.LoginUserDto;
import com.mars.springboot.shop.service.LoginUserService;
import com.mars.springboot.shop.vo.LoginUserVo;
import com.mars.springboot.shop.vo.ResVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 登录操作
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@RestController
@CrossOrigin
public class LoginUserController {


    @Autowired
    private LoginUserService loginUserService;

//    @PostMapping(value = "/login",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public ResVo login(@RequestBody LoginUserDto loginUserDto){
//        ResVo resVo = new ResVo(StatusCode.Success);
//         //判断是否账号和密码是否有问题
//        if(StringUtils.isBlank(loginUserDto.getUsername())||StringUtils.isBlank(loginUserDto.getPassword())){
//            return new ResVo(StatusCode.IsBlankFail);
//        }
//
//        LoginUserVo res = this.loginUserService.queryLoginUser(loginUserDto.getUsername(),loginUserDto.getPassword());
//        return new ResVo(StatusCode.Success,res);
//    }


    @PostMapping(value = "/login",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResVo login(@Validated @RequestBody LoginUserDto loginUserDto){
        ResVo resVo = new ResVo(StatusCode.OK);
        //判断是否账号和密码是否有问题
        if(StringUtils.isBlank(loginUserDto.getUsername())||StringUtils.isBlank(loginUserDto.getPassword())){
            return new ResVo(StatusCode.IsBlankFail);
        }

        LoginUserVo res = this.loginUserService.queryLoginUser(loginUserDto.getUsername(),loginUserDto.getPassword());

        if(StringUtils.isEmpty(res.getToken())){
            return new ResVo(StatusCode.IsErrorUserOrPassword,res);
        }
        resVo.setData(res);
        resVo.setMeta(StatusCode.OK);

        return resVo;
    }


    @PostMapping(value = "/loginForm",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResVo login2(@RequestBody LoginUserDto loginUserDto){
        ResVo resVo = new ResVo(StatusCode.OK);
        //判断是否账号和密码是否有问题
        if(StringUtils.isBlank(loginUserDto.getUsername())||StringUtils.isBlank(loginUserDto.getPassword())){
            return new ResVo(StatusCode.IsBlankFail);
        }

        LoginUserVo res = this.loginUserService.queryLoginUser(loginUserDto.getUsername(),loginUserDto.getPassword());

        if(StringUtils.isEmpty(res.getToken())){
            return new ResVo(StatusCode.IsErrorUserOrPassword,res);
        }

        resVo.setData(res);
        resVo.setMeta(StatusCode.OK);

        return resVo;
    }



}
