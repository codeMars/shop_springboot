package com.mars.springboot.shop.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mars.springboot.shop.base.StatusCode;
import com.mars.springboot.shop.service.MenusService;
import com.mars.springboot.shop.service.SpPermissionApiService;
import com.mars.springboot.shop.service.SpPermissionService;
import com.mars.springboot.shop.vo.MenusVo;
import com.mars.springboot.shop.vo.ResVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin  //解决跨域问题
public class MenusController {

    @Autowired
    private SpPermissionService spPermissionService;

    @Autowired
    private MenusService menusService;

    @Autowired
    private SpPermissionApiService spPermissionApiService;

    @GetMapping("/menus2")   // 树形方式1
    public ResVo getMenus(){
    //返回：
    //{"data":[{"id":125,"authName":"用户管理","path":"users","children":
        // [{"id":110,"authName":"用户列表","path":"users","children":[],"order":null}],
        // "order":1},{"id":103,"authName":"权限管理","path":"rights","children":[{"id":111,"authName":"角色列表",
        // "path":"roles","children":[],"order":null},{"id":112,"authName":"权限列表","path":"rights","children":[],
        // "order":null}],"order":2},{"id":101,"authName":"商品管理","path":"goods","children":
        // [{"id":104,"authName":"商品列表","path":"goods","children":[],"order":1},{"id":115,"authName":"分类参数",
        // "path":"params","children":[],"order":2},{"id":121,"authName":"商品分类","path":"categories","children":[],
        // "order":3}],"order":3},{"id":102,"authName":"订单管理","path":"orders","children":[{"id":107,"authName":
        // "订单列表","path":"orders","children":[],"order":null}],"order":4},{"id":145,"authName":"数据统计","path":
        // "reports","children":[{"id":146,"authName":"数据报表",
        // "path":"reports","children":[],"order":null}],"order":5}],"meta":{"msg":"获取菜单列表成功","status":200}}
        ResVo resVo = new ResVo(StatusCode.Init);

        List<MenusVo> a = menusService.getMainMenu();
        System.out.println("QQQQQQQQQQQQQQQQQ");
        System.out.println(a);
        System.out.println(JSON.toJSONString(a));
        return resVo;
    }


    @GetMapping("/menus")  // 树形方式2
    public ResVo getMenus2(){
        //返回：
        //{"data":[{"id":125,"authName":"用户管理","path":"users","children":
        // [{"id":110,"authName":"用户列表","path":"users","children":[],"order":null}],
        // "order":1},{"id":103,"authName":"权限管理","path":"rights","children":[{"id":111,"authName":"角色列表",
        // "path":"roles","children":[],"order":null},{"id":112,"authName":"权限列表","path":"rights","children":[],
        // "order":null}],"order":2},{"id":101,"authName":"商品管理","path":"goods","children":
        // [{"id":104,"authName":"商品列表","path":"goods","children":[],"order":1},{"id":115,"authName":"分类参数",
        // "path":"params","children":[],"order":2},{"id":121,"authName":"商品分类","path":"categories","children":[],
        // "order":3}],"order":3},{"id":102,"authName":"订单管理","path":"orders","children":[{"id":107,"authName":
        // "订单列表","path":"orders","children":[],"order":null}],"order":4},{"id":145,"authName":"数据统计","path":
        // "reports","children":[{"id":146,"authName":"数据报表",
        // "path":"reports","children":[],"order":null}],"order":5}],"meta":{"msg":"获取菜单列表成功","status":200}}
        ResVo resVo = new ResVo(StatusCode.Init);

        List<MenusVo> a = menusService.menuGetTree();
        System.out.println("QQQQQQQQQQQQQQQQQ");
        System.out.println(a);
        System.out.println(JSON.toJSONString(a));

        resVo.setData(a);
        resVo.setMeta(StatusCode.OK);

        return resVo;
    }

}
