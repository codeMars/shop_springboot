package com.mars.springboot.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class RedisController {



    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping(value = "/redis")
    public void setRedis(){
        redisTemplate.opsForValue().set("key","ok");

    }
}
