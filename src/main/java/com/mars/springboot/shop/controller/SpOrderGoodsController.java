package com.mars.springboot.shop.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品订单关联表 前端控制器
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@RestController
@RequestMapping("/sp-order-goods-entity")
public class SpOrderGoodsController {

}
