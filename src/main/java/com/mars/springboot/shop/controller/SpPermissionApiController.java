package com.mars.springboot.shop.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@RestController
@RequestMapping("/sp-permission-api-entity")
public class SpPermissionApiController {

}
