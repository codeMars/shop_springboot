package com.mars.springboot.shop.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import com.mars.springboot.shop.base.StatusCode;
import com.mars.springboot.shop.dao.SpManagerDao;
import com.mars.springboot.shop.dao.SpUserDao;
import com.mars.springboot.shop.dao.UserListDao;
import com.mars.springboot.shop.dto.AddUserDto;
import com.mars.springboot.shop.dto.UserListDto;
import com.mars.springboot.shop.entity.SpManagerEntity;
import com.mars.springboot.shop.entity.SpUserEntity;
import com.mars.springboot.shop.entity.UserEntity;
import com.mars.springboot.shop.service.SpManagerService;
import com.mars.springboot.shop.service.SpUserService;
import com.mars.springboot.shop.vo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Map;


@RestController
@CrossOrigin
public class UserController {

    @Autowired
    private SpManagerService spManagerService;

    @Autowired
    private UserListDao userListDao;

    @Autowired
    private SpManagerDao spManagerDao;


    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @GetMapping("/users")
    public ResVo getUsers(@Validated UserListDto userListDto, BindingResult result){

        ResVo resVo = new ResVo(StatusCode.Init);
        //字段校验
        if (result.hasErrors()) {
            resVo.setMeta(StatusCode.InvalidParams);
            resVo.setData(result.getFieldError().getDefaultMessage());
            return resVo;
        }

        //分页功能
            //定义页面信息
        Page<UserEntity> page = new Page<>(userListDto.getPagenum(), userListDto.getPagesize());
        IPage<UserEntity> iPage = userListDao.getUserListPageVo(page);
        System.out.println("返回的内容信息："+iPage.getRecords());  //返回的内容信息
        System.out.println("总记录数："+iPage.getTotal());  //总记录数
        System.out.println("当前页码："+iPage.getCurrent()); //当前页码
        System.out.println("每页显示条数："+iPage.getSize());   //每页显示条数
        System.out.println("总页数："+userListDto.getPagenum());  //总页数
        UserListVo userListVo = new UserListVo();
        userListVo.setTotal(iPage.getTotal());
        userListVo.setPagenum(iPage.getCurrent());
        userListVo.setUsers(iPage.getRecords());

        resVo.setData(userListVo);
        resVo.setMeta(StatusCode.OK);

        return resVo;

    }



    @PostMapping("/users")
    public ResVo addUser(@RequestBody @Validated AddUserDto addUserDto,BindingResult result){
        ResVo resVo = new ResVo(StatusCode.Init);

        //字段校验
        if (result.hasErrors()) {
            resVo.setMeta(StatusCode.InvalidParams);
            resVo.setData(result.getFieldError().getDefaultMessage());
            return resVo;
        }

        //用户名唯一判断
        int i = this.spManagerService.count(new QueryWrapper<SpManagerEntity>().eq("mg_name",addUserDto.getUsername()));
        if(i>0){
            resVo.setMeta(StatusCode.AccountValidateError);
            return resVo;
        }

        //正式进入提交操作
        AddUserVo addUserVo = this.spManagerService.addUser(addUserDto);
        if(addUserVo == null){
            resVo.setMeta(StatusCode.InserError);
            return resVo;
        }

        resVo.setData(addUserVo);
        resVo.setMeta(StatusCode.CREATED);

        return resVo;
    }


    @PutMapping("/users/{uId}/state/{type}")
    public ResVo updateUserState(@PathVariable Integer uId,@PathVariable Boolean type){

        ResVo resVo = new ResVo(StatusCode.Init);

        if(uId <=0){
            resVo.setMeta(StatusCode.Fail);
            return resVo;
        }

        UpdateUserStateVo updateUserStateVo = this.spManagerService.updateUserState(uId,type);
        if(updateUserStateVo == null ||updateUserStateVo.equals("")){
            resVo.setMeta(StatusCode.Fail);
            return resVo;
        }

        resVo.setData(updateUserStateVo);
        resVo.setMeta(StatusCode.OK);

        return resVo;
    }


    @GetMapping("/users/{id}")
    public ResVo getUserById(@PathVariable Integer id){

        ResVo resVo = new ResVo(StatusCode.Init);
        if(id < 0 || id == null){
            resVo.setMeta(StatusCode.InvalidParams);
            return resVo;
        }

        UserByIdVo userByIdVo = this.spManagerService.getUserInfoById(id);
        if(userByIdVo == null ||userByIdVo.equals("")){
            resVo.setMeta(StatusCode.Fail);
            return resVo;
        }

        resVo.setData(userByIdVo);
        resVo.setMeta(StatusCode.OK);

        return resVo;

    }


    @PutMapping("/users/{id}")
    public ResVo updateUserInfo(@PathVariable Integer id,@RequestBody Map<String,Object> paraMap){

        ResVo resVo = new ResVo(StatusCode.Init);
        if(id < 0 || id == null){
            resVo.setMeta(StatusCode.InvalidParams);
            return resVo;
        }

        if(this.spManagerService.selectCount("mg_id",String.valueOf(id)) <= 0 ){
            logger.error("查询结果为空");
            resVo.setMeta(StatusCode.selectIsBank);
            return resVo;
        }

        UpdateUserVo updateUserVo = this.spManagerService.updateUserInfo(id,paraMap);


        resVo.setData(updateUserVo);
        resVo.setMeta(StatusCode.OK);

        return resVo;

    }


    @DeleteMapping("/users/{id}")
    public ResVo updateUserInfo(@PathVariable Integer id){

        ResVo resVo = new ResVo(StatusCode.Init);
        if(id < 0 || id == null){
            resVo.setMeta(StatusCode.InvalidParams);
            return resVo;
        }

        if(this.spManagerDao.deleteById(id) <= 0){
            resVo.setMeta(StatusCode.InvalidParams.getCode(),"删除失败，请重新再试");
        }

        resVo.setMeta(StatusCode.OK);

        return resVo;

    }

}
