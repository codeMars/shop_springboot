package com.mars.springboot.shop.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.springboot.shop.dto.LoginUserDto;
import com.mars.springboot.shop.entity.SpCategoryEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Mapper
public interface LoginUserDao extends BaseMapper<LoginUserDto> {


//    LoginUserDto queryLoginUser(@Param("username") String username, @Param("password") String password);

}
