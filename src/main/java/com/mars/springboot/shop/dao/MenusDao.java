package com.mars.springboot.shop.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.springboot.shop.entity.SpPermissionEntity;
import com.mars.springboot.shop.vo.MenusVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MenusDao  extends BaseMapper<MenusVo> {

    List<MenusVo> queryRootMenus();
    List<MenusVo> getNextNodeTree();
    List<SpPermissionEntity> queryAlltMenus();

}
