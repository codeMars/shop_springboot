package com.mars.springboot.shop.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.springboot.shop.entity.SpConsigneeEntity;

/**
 * <p>
 * 收货人表 Mapper 接口
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpConsigneeDao extends BaseMapper<SpConsigneeEntity> {

}
