package com.mars.springboot.shop.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.springboot.shop.entity.SpGoodsAttrEntity;

/**
 * <p>
 * 商品-属性关联表 Mapper 接口
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpGoodsAttrDao extends BaseMapper<SpGoodsAttrEntity> {

}
