package com.mars.springboot.shop.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.springboot.shop.entity.SpManagerEntity;
import com.mars.springboot.shop.entity.UserEntity;
import com.mars.springboot.shop.vo.UserListVo;

/**
 * <p>
 * 管理员表 Mapper 接口
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpManagerDao extends BaseMapper<SpManagerEntity> {



}
