package com.mars.springboot.shop.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.springboot.shop.entity.SpOrderGoodsEntity;

/**
 * <p>
 * 商品订单关联表 Mapper 接口
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpOrderGoodsDao extends BaseMapper<SpOrderGoodsEntity> {

}
