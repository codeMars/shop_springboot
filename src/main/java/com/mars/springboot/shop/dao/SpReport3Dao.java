package com.mars.springboot.shop.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.springboot.shop.entity.SpReport3Entity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpReport3Dao extends BaseMapper<SpReport3Entity> {

}
