package com.mars.springboot.shop.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.springboot.shop.entity.SpUserCartEntity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpUserCartDao extends BaseMapper<SpUserCartEntity> {

}
