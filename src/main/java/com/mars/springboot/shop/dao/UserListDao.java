package com.mars.springboot.shop.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.springboot.shop.entity.UserEntity;
import com.mars.springboot.shop.vo.UserListVo;

public interface UserListDao extends BaseMapper<UserEntity> {

    IPage<UserEntity> getUserListPageVo(Page<UserEntity> page);

}
