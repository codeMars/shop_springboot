package com.mars.springboot.shop.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class LoginUserDto implements Serializable {

    //姓名
    @NotBlank(message = "用户名不能为空")
    private String username;

    //密码
    @NotNull(message = "密码不能为空")
    private String password;


}
