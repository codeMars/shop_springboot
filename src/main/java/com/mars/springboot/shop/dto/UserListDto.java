package com.mars.springboot.shop.dto;

import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;

@Data
public class UserListDto implements Serializable {


    private String query;

    @NotNull(message = "当前页码不能为空")
    private Integer pagenum;   //当前页码

    @NotNull(message = "每页显示条数不能为空")
    @Min(1)
    @Max(100)
    private Integer pagesize;   //每页显示条数

}
