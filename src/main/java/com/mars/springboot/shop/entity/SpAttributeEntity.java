package com.mars.springboot.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 属性表
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sp_attribute")
public class SpAttributeEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "attr_id", type = IdType.AUTO)
    private Integer attrId;

    /**
     * 属性名称
     */
    @TableField("attr_name")
    private String attrName;

    /**
     * 外键，类型id
     */
    @TableField("cat_id")
    private Integer catId;

    /**
     * only:输入框(唯一)  many:后台下拉列表/前台单选框
     */
    @TableField("attr_sel")
    private String attrSel;

    /**
     * manual:手工录入  list:从列表选择
     */
    @TableField("attr_write")
    private String attrWrite;

    /**
     * 可选值列表信息,例如颜色：白色,红色,绿色,多个可选值通过逗号分隔
     */
    @TableField("attr_vals")
    private String attrVals;

    /**
     * 删除时间标志
     */
    @TableField("delete_time")
    private Integer deleteTime;


}
