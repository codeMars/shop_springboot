package com.mars.springboot.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sp_category")
public class SpCategoryEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 分类唯一ID
     */
    @TableId(value = "cat_id", type = IdType.AUTO)
    private Integer catId;

    /**
     * 分类名称
     */
    @TableField("cat_name")
    private String catName;

    /**
     * 分类父ID
     */
    @TableField("cat_pid")
    private Integer catPid;

    /**
     * 分类层级 0: 顶级 1:二级 2:三级
     */
    @TableField("cat_level")
    private Integer catLevel;

    /**
     * 是否删除 1为删除
     */
    @TableField("cat_deleted")
    private Integer catDeleted;

    @TableField("cat_icon")
    private String catIcon;

    @TableField("cat_src")
    private String catSrc;


}
