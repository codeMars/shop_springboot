package com.mars.springboot.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 收货人表
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sp_consignee")
public class SpConsigneeEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "cgn_id", type = IdType.AUTO)
    private Integer cgnId;

    /**
     * 会员id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 收货人名称
     */
    @TableField("cgn_name")
    private String cgnName;

    /**
     * 收货人地址
     */
    @TableField("cgn_address")
    private String cgnAddress;

    /**
     * 收货人电话
     */
    @TableField("cgn_tel")
    private String cgnTel;

    /**
     * 邮编
     */
    @TableField("cgn_code")
    private String cgnCode;

    /**
     * 删除时间
     */
    @TableField("delete_time")
    private Integer deleteTime;


}
