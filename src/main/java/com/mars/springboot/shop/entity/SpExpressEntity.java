package com.mars.springboot.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 快递表
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sp_express")
public class SpExpressEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "express_id", type = IdType.AUTO)
    private Integer expressId;

    /**
     * 订单id
     */
    @TableField("order_id")
    private Integer orderId;

    /**
     * 订单快递公司名称
     */
    @TableField("express_com")
    private String expressCom;

    /**
     * 快递单编号
     */
    @TableField("express_nu")
    private String expressNu;

    /**
     * 记录生成时间
     */
    @TableField("create_time")
    private Integer createTime;

    /**
     * 记录修改时间
     */
    @TableField("update_time")
    private Integer updateTime;


}
