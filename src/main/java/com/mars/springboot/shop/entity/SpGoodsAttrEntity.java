package com.mars.springboot.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 商品-属性关联表
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sp_goods_attr")
public class SpGoodsAttrEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品id
     */
    @TableField("goods_id")
    private Integer goodsId;

    /**
     * 属性id
     */
    @TableField("attr_id")
    private Integer attrId;

    /**
     * 商品对应属性的值
     */
    @TableField("attr_value")
    private String attrValue;

    /**
     * 该属性需要额外增加的价钱
     */
    @TableField("add_price")
    private BigDecimal addPrice;


}
