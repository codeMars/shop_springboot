package com.mars.springboot.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sp_goods")
public class SpGoodsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "goods_id", type = IdType.AUTO)
    private Integer goodsId;

    /**
     * 商品名称
     */
    @TableField("goods_name")
    private String goodsName;

    /**
     * 商品价格
     */
    @TableField("goods_price")
    private BigDecimal goodsPrice;

    /**
     * 商品数量
     */
    @TableField("goods_number")
    private Integer goodsNumber;

    /**
     * 商品重量
     */
    @TableField("goods_weight")
    private Integer goodsWeight;

    /**
     * 类型id
     */
    @TableField("cat_id")
    private Integer catId;

    /**
     * 商品详情介绍
     */
    @TableField("goods_introduce")
    private String goodsIntroduce;

    /**
     * 图片logo大图
     */
    @TableField("goods_big_logo")
    private String goodsBigLogo;

    /**
     * 图片logo小图
     */
    @TableField("goods_small_logo")
    private String goodsSmallLogo;

    /**
     * 0:正常  1:删除
     */
    @TableField("is_del")
    private String isDel;

    /**
     * 添加商品时间
     */
    @TableField("add_time")
    private Integer addTime;

    /**
     * 修改商品时间
     */
    @TableField("upd_time")
    private Integer updTime;

    /**
     * 软删除标志字段
     */
    @TableField("delete_time")
    private Integer deleteTime;

    /**
     * 一级分类id
     */
    @TableField("cat_one_id")
    private Integer catOneId;

    /**
     * 二级分类id
     */
    @TableField("cat_two_id")
    private Integer catTwoId;

    /**
     * 三级分类id
     */
    @TableField("cat_three_id")
    private Integer catThreeId;

    /**
     * 热卖数量
     */
    @TableField("hot_mumber")
    private Integer hotMumber;

    /**
     * 是否促销
     */
    @TableField("is_promote")
    private Integer isPromote;

    /**
     * 商品状态 0: 未通过 1: 审核中 2: 已审核
     */
    @TableField("goods_state")
    private Integer goodsState;


}
