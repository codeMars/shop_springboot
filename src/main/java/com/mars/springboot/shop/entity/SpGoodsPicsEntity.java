package com.mars.springboot.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 商品-相册关联表
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sp_goods_pics")
public class SpGoodsPicsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "pics_id", type = IdType.AUTO)
    private Integer picsId;

    /**
     * 商品id
     */
    @TableField("goods_id")
    private Integer goodsId;

    /**
     * 相册大图800*800
     */
    @TableField("pics_big")
    private String picsBig;

    /**
     * 相册中图350*350
     */
    @TableField("pics_mid")
    private String picsMid;

    /**
     * 相册小图50*50
     */
    @TableField("pics_sma")
    private String picsSma;


}
