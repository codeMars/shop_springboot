package com.mars.springboot.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 管理员表
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sp_manager")
public class SpManagerEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "mg_id", type = IdType.AUTO)
    private Integer mgId;

    /**
     * 名称
     */
    @TableField("mg_name")
    private String mgName;

    /**
     * 密码
     */
    @TableField("mg_pwd")
    private String mgPwd;

    /**
     * 注册时间
     */
    @TableField("mg_time")
    private Integer mgTime;

    /**
     * 角色id
     */
    @TableField("role_id")
    private Integer roleId;

    @TableField("mg_mobile")
    private String mgMobile;

    @TableField("mg_email")
    private String mgEmail;

    /**
     * 1：表示启用 0:表示禁用
     */
    @TableField("mg_state")
    private Integer mgState;


}
