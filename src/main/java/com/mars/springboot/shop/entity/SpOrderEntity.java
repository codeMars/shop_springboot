package com.mars.springboot.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sp_order")
public class SpOrderEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "order_id", type = IdType.AUTO)
    private Integer orderId;

    /**
     * 下订单会员id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 订单编号
     */
    @TableField("order_number")
    private String orderNumber;

    /**
     * 订单总金额
     */
    @TableField("order_price")
    private BigDecimal orderPrice;

    /**
     * 支付方式  0未支付 1支付宝  2微信  3银行卡
     */
    @TableField("order_pay")
    private String orderPay;

    /**
     * 订单是否已经发货
     */
    @TableField("is_send")
    private String isSend;

    /**
     * 支付宝交易流水号码
     */
    @TableField("trade_no")
    private String tradeNo;

    /**
     * 发票抬头 个人 公司
     */
    @TableField("order_fapiao_title")
    private String orderFapiaoTitle;

    /**
     * 公司名称
     */
    @TableField("order_fapiao_company")
    private String orderFapiaoCompany;

    /**
     * 发票内容
     */
    @TableField("order_fapiao_content")
    private String orderFapiaoContent;

    /**
     * consignee收货人地址
     */
    @TableField("consignee_addr")
    private String consigneeAddr;

    /**
     * 订单状态： 0未付款、1已付款
     */
    @TableField("pay_status")
    private String payStatus;

    /**
     * 记录生成时间
     */
    @TableField("create_time")
    private Integer createTime;

    /**
     * 记录修改时间
     */
    @TableField("update_time")
    private Integer updateTime;


}
