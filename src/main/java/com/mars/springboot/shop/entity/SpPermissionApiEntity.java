package com.mars.springboot.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sp_permission_api")
public class SpPermissionApiEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("ps_id")
    private Integer psId;

    @TableField("ps_api_service")
    private String psApiService;

    @TableField("ps_api_action")
    private String psApiAction;

    @TableField("ps_api_path")
    private String psApiPath;

    @TableField("ps_api_order")
    private Integer psApiOrder;


}
