package com.mars.springboot.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 权限表
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sp_permission")
public class SpPermissionEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ps_id", type = IdType.AUTO)
    private Integer psId;

    /**
     * 权限名称
     */
    @TableField("ps_name")
    private String psName;

    /**
     * 父id
     */
    @TableField("ps_pid")
    private Integer psPid;

    /**
     * 控制器
     */
    @TableField("ps_c")
    private String psC;

    /**
     * 操作方法
     */
    @TableField("ps_a")
    private String psA;

    /**
     * 权限等级
     */
    @TableField("ps_level")
    private String psLevel;


    @TableField(exist=false) //数据库不存在
    private String psApiPath;

    @TableField(exist=false) //数据库不存在
    private Integer psApiOrder;
}
