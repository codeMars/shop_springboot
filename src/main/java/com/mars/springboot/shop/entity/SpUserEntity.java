package com.mars.springboot.shop.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sp_user")
public class SpUserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;

    /**
     * 登录名
     */
    @TableField("username")
    private String username;

    /**
     * qq官方唯一编号信息
     */
    @TableField("qq_open_id")
    private String qqOpenId;

    /**
     * 登录密码
     */
    @TableField("password")
    private String password;

    /**
     * 邮箱
     */
    @TableField("user_email")
    private String userEmail;

    /**
     * 新用户注册邮件激活唯一校验码
     */
    @TableField("user_email_code")
    private String userEmailCode;

    /**
     * 新用户是否已经通过邮箱激活帐号
     */
    @TableField("is_active")
    private String isActive;

    /**
     * 性别
     */
    @TableField("user_sex")
    private String userSex;

    /**
     * qq
     */
    @TableField("user_qq")
    private String userQq;

    /**
     * 手机
     */
    @TableField("user_tel")
    private String userTel;

    /**
     * 学历
     */
    @TableField("user_xueli")
    private String userXueli;

    /**
     * 爱好
     */
    @TableField("user_hobby")
    private String userHobby;

    /**
     * 简介
     */
    @TableField("user_introduce")
    private String userIntroduce;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Integer createTime;

    /**
     * 修改时间
     */
    @TableField("update_time")
    private Integer updateTime;


}
