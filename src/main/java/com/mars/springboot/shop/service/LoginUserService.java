package com.mars.springboot.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.shop.dto.LoginUserDto;
import com.mars.springboot.shop.vo.LoginUserVo;


public interface LoginUserService extends IService<LoginUserDto> {

    LoginUserVo queryLoginUser(String username, String password);

}
