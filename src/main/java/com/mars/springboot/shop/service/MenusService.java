package com.mars.springboot.shop.service;


import com.mars.springboot.shop.entity.SpPermissionEntity;
import com.mars.springboot.shop.vo.MenusVo;

import java.util.List;

public interface MenusService {

    List<MenusVo> getMainMenu();

    List<MenusVo> menuGetTree();

    List<MenusVo> menuNextTree(List<SpPermissionEntity> list, int nodeId);



}
