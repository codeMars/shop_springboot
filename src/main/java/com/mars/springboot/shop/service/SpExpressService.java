package com.mars.springboot.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.shop.entity.SpExpressEntity;

/**
 * <p>
 * 快递表 服务类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpExpressService extends IService<SpExpressEntity> {

}
