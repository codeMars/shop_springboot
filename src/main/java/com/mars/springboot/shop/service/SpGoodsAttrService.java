package com.mars.springboot.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.shop.entity.SpGoodsAttrEntity;

/**
 * <p>
 * 商品-属性关联表 服务类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpGoodsAttrService extends IService<SpGoodsAttrEntity> {

}
