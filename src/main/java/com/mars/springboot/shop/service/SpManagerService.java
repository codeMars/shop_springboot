package com.mars.springboot.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.shop.dto.AddUserDto;
import com.mars.springboot.shop.entity.SpManagerEntity;
import com.mars.springboot.shop.vo.AddUserVo;
import com.mars.springboot.shop.vo.UpdateUserStateVo;
import com.mars.springboot.shop.vo.UpdateUserVo;
import com.mars.springboot.shop.vo.UserByIdVo;

import java.util.Map;

/**
 * <p>
 * 管理员表 服务类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpManagerService extends IService<SpManagerEntity> {

    AddUserVo addUser(AddUserDto addUserDto);

    UpdateUserStateVo updateUserState(Integer uId,Boolean type);

    UserByIdVo getUserInfoById(Integer id);

    int selectCount(String field,String key);

    UpdateUserVo updateUserInfo(Integer id, Map<String,Object> paraMap);

}
