package com.mars.springboot.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.shop.entity.SpOrderGoodsEntity;

/**
 * <p>
 * 商品订单关联表 服务类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpOrderGoodsService extends IService<SpOrderGoodsEntity> {

}
