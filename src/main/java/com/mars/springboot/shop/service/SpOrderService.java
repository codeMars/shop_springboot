package com.mars.springboot.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.shop.entity.SpOrderEntity;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpOrderService extends IService<SpOrderEntity> {

}
