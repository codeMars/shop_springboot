package com.mars.springboot.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.shop.entity.SpReport2Entity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpReport2Service extends IService<SpReport2Entity> {

}
