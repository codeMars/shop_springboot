package com.mars.springboot.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.shop.entity.SpReport3Entity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpReport3Service extends IService<SpReport3Entity> {

}
