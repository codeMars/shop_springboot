package com.mars.springboot.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.shop.entity.SpRoleEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpRoleService extends IService<SpRoleEntity> {

}
