package com.mars.springboot.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.springboot.shop.entity.SpUserEntity;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
public interface SpUserService extends IService<SpUserEntity> {


}
