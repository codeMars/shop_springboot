package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.LoginUserDao;
import com.mars.springboot.shop.dto.LoginUserDto;
import com.mars.springboot.shop.entity.SpManagerEntity;
import com.mars.springboot.shop.entity.SpUserEntity;
import com.mars.springboot.shop.service.LoginUserService;
import com.mars.springboot.shop.service.SpManagerService;
import com.mars.springboot.shop.service.SpUserService;
import com.mars.springboot.shop.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("loginUserService")
public class LoginUserServiceImpl extends ServiceImpl<LoginUserDao,LoginUserDto> implements LoginUserService {

    @Autowired
    private SpManagerService spManagerService;

    @Autowired
    private SpUserService spUserService;

    @Override
    public LoginUserVo queryLoginUser(String username, String password) {

        LoginUserVo loginUserVo = new LoginUserVo();
        List<SpManagerEntity> spManagerEntityList = this.spManagerService.list(new QueryWrapper<SpManagerEntity>()
                .and(i->i.eq("mg_name",username).eq("mg_pwd",password)));
        if(spManagerEntityList.size() >0){
            SpManagerEntity spManagerEntity = spManagerEntityList.get(0);
            loginUserVo.setId(spManagerEntity.getMgId());
            loginUserVo.setUsername(spManagerEntity.getMgName());
            loginUserVo.setEmail(spManagerEntity.getMgEmail());
            loginUserVo.setMobile(Integer.valueOf(spManagerEntity.getMgMobile()));
            loginUserVo.setRid(spManagerEntity.getRoleId());
            loginUserVo.setToken("10086");
        }else{
            List<SpUserEntity> spUserEntityList = this.spUserService.list(new QueryWrapper<SpUserEntity>()
                    .and(i->i.eq("username",username).eq("password",password)));

            if(spUserEntityList.size()>0){
                SpUserEntity spUserEntity = spUserEntityList.get(0);
                loginUserVo.setRid(spUserEntity.getUserId());
                loginUserVo.setEmail(spUserEntity.getUserEmail());
                loginUserVo.setId(spUserEntity.getUserId());
                loginUserVo.setMobile(Integer.valueOf(spUserEntity.getUserTel()));
                loginUserVo.setToken("10086");
            }
        }


        return loginUserVo;
    }
}
