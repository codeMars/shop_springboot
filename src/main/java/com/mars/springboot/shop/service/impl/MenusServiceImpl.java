package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.MenusDao;
import com.mars.springboot.shop.dao.SpPermissionDao;
import com.mars.springboot.shop.entity.SpPermissionEntity;
import com.mars.springboot.shop.service.MenusService;
import com.mars.springboot.shop.vo.MenusVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("menusService")
public class MenusServiceImpl extends ServiceImpl<MenusDao,MenusVo> implements MenusService {


    @Autowired
    private MenusDao menusDao;

    @Autowired
    private SpPermissionDao spPermissionDao;

    @Override
    public List<MenusVo> getMainMenu() {
        List<MenusVo> v =menusDao.queryRootMenus();
        System.out.println(v);
        return v;
    }


    @Override
    public List<MenusVo> menuGetTree() {
        QueryWrapper queryWrapper = new QueryWrapper();
        //查询所有的菜单
 //       List<SpPermissionEntity> list = spPermissionDao.selectList(queryWrapper);
        List<SpPermissionEntity> list = menusDao.queryAlltMenus();
        List<MenusVo> list1 = new ArrayList();
        for(SpPermissionEntity spPermissionEntity : list){
            System.out.println("spPermissionEntity: "+spPermissionEntity);
            MenusVo menusVo = new MenusVo();
            if(spPermissionEntity.getPsPid().equals(0)){
//                //把spPermissionEntity的属性 复制给menusVo
//                BeanUtils.copyProperties(spPermissionEntity, menusVo);
                menusVo.setId(spPermissionEntity.getPsId());
                menusVo.setAuthName(spPermissionEntity.getPsName());
                menusVo.setOrder(spPermissionEntity.getPsApiOrder());
                menusVo.setPath(spPermissionEntity.getPsApiPath());
                menusVo.setChildren(menuNextTree(list,(spPermissionEntity.getPsId())));
                list1.add(menusVo);
            }
        }
        return list1;
    }

    //只获取第一级和第二级数据列表
    @Override
    public List<MenusVo> menuNextTree(List<SpPermissionEntity> list, int nodeId) {
        List<MenusVo> list1 = new ArrayList();
        for(SpPermissionEntity spPermissionEntity : list){
            MenusVo menusVo = new MenusVo();
            if(spPermissionEntity.getPsPid().equals(nodeId)){

//              BeanUtils.copyProperties(spPermissionEntity, menusVo);

                menusVo.setId(spPermissionEntity.getPsId());
                menusVo.setAuthName(spPermissionEntity.getPsName());
                menusVo.setPath(spPermissionEntity.getPsApiPath());
                menusVo.setOrder(spPermissionEntity.getPsApiOrder());
                menusVo.setChildren(null);
                //无限多层树形结构迭代函数
//                menusVo.setChildren(menuNextTree(list,spPermissionEntity.getPsId()));
                list1.add(menusVo);
            }
        }
        return list1;
    }

//    无限循环迭代～～树形结构列表
//    @Override
//    public List<MenusVo> menuNextTree(List<SpPermissionEntity> list, int nodeId) {
//        List<MenusVo> list1 = new ArrayList();
//        for(SpPermissionEntity spPermissionEntity : list){
//            MenusVo menusVo = new MenusVo();
//            if(spPermissionEntity.getPsPid().equals(nodeId)){
//
////              BeanUtils.copyProperties(spPermissionEntity, menusVo);
//
//                menusVo.setId(spPermissionEntity.getPsId());
//                menusVo.setAuthName(spPermissionEntity.getPsName());
//                menusVo.setPath("www.222.com");
//                menusVo.setChildren(null);
//                //无限多层树形结构迭代函数
////                menusVo.setChildren(formulaClothClassNextTree(list,spPermissionEntity.getPsId()));
//                list1.add(menusVo);
//            }
//        }
//        return list1;
//    }
}
