package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpAttributeDao;
import com.mars.springboot.shop.entity.SpAttributeEntity;
import com.mars.springboot.shop.service.SpAttributeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 属性表 服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpAttributeServiceImpl extends ServiceImpl<SpAttributeDao, SpAttributeEntity> implements SpAttributeService {

}
