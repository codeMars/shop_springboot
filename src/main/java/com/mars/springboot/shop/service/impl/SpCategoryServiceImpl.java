package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpCategoryDao;
import com.mars.springboot.shop.entity.SpCategoryEntity;
import com.mars.springboot.shop.service.SpCategoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpCategoryServiceImpl extends ServiceImpl<SpCategoryDao, SpCategoryEntity> implements SpCategoryService {

}
