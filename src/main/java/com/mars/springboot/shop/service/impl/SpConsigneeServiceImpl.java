package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpConsigneeDao;
import com.mars.springboot.shop.entity.SpConsigneeEntity;
import com.mars.springboot.shop.service.SpConsigneeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收货人表 服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpConsigneeServiceImpl extends ServiceImpl<SpConsigneeDao, SpConsigneeEntity> implements SpConsigneeService {

}
