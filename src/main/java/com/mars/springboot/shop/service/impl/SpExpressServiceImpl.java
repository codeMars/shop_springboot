package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpExpressDao;
import com.mars.springboot.shop.entity.SpExpressEntity;
import com.mars.springboot.shop.service.SpExpressService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 快递表 服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpExpressServiceImpl extends ServiceImpl<SpExpressDao, SpExpressEntity> implements SpExpressService {

}
