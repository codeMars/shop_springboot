package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpGoodsAttrDao;
import com.mars.springboot.shop.entity.SpGoodsAttrEntity;
import com.mars.springboot.shop.service.SpGoodsAttrService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品-属性关联表 服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpGoodsAttrServiceImpl extends ServiceImpl<SpGoodsAttrDao, SpGoodsAttrEntity> implements SpGoodsAttrService {

}
