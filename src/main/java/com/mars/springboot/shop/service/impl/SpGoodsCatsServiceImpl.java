package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpGoodsCatsDao;
import com.mars.springboot.shop.entity.SpGoodsCatsEntity;
import com.mars.springboot.shop.service.SpGoodsCatsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpGoodsCatsServiceImpl extends ServiceImpl<SpGoodsCatsDao, SpGoodsCatsEntity> implements SpGoodsCatsService {

}
