package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpGoodsPicsDao;
import com.mars.springboot.shop.entity.SpGoodsPicsEntity;
import com.mars.springboot.shop.service.SpGoodsPicsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品-相册关联表 服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpGoodsPicsServiceImpl extends ServiceImpl<SpGoodsPicsDao, SpGoodsPicsEntity> implements SpGoodsPicsService {

}
