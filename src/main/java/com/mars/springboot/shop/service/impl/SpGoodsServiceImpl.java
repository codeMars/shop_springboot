package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpGoodsDao;
import com.mars.springboot.shop.entity.SpGoodsEntity;
import com.mars.springboot.shop.service.SpGoodsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpGoodsServiceImpl extends ServiceImpl<SpGoodsDao, SpGoodsEntity> implements SpGoodsService {

}
