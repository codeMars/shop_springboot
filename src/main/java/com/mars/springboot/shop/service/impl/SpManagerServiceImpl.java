package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpManagerDao;
import com.mars.springboot.shop.dto.AddUserDto;
import com.mars.springboot.shop.entity.SpManagerEntity;
import com.mars.springboot.shop.service.SpManagerService;
import com.mars.springboot.shop.vo.AddUserVo;
import com.mars.springboot.shop.vo.UpdateUserStateVo;
import com.mars.springboot.shop.vo.UpdateUserVo;
import com.mars.springboot.shop.vo.UserByIdVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.BeanUtils;

import java.util.Map;

/**
 * <p>
 * 管理员表 服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpManagerServiceImpl extends ServiceImpl<SpManagerDao, SpManagerEntity> implements SpManagerService {


    @Autowired
    private SpManagerDao spManagerDao;


    //新增用户
    @Override
    @Transactional(rollbackFor = Exception.class)
    public AddUserVo addUser(AddUserDto addUserDto){
        AddUserVo addUserVo = new AddUserVo();
        SpManagerEntity entity = new SpManagerEntity();
        entity.setMgEmail(addUserDto.getEmail());
        entity.setMgName(addUserDto.getUsername());
        entity.setMgMobile(addUserDto.getMobile());
        entity.setMgPwd(addUserDto.getPassword());
        entity.setMgState(1);
        entity.setMgTime(10086);
        entity.setRoleId(42);
        if(spManagerDao.insert(entity) == 0){
            return null;
        }
        addUserVo.setId(entity.getMgId());
        addUserVo.setEmail(entity.getMgEmail());
        addUserVo.setCreate_time(entity.getMgTime().toString());
        addUserVo.setIs_active(entity.getMgState() == 0 ? false:true);
        addUserVo.setMobile(entity.getMgMobile());
        addUserVo.setUsername(entity.getMgName());
        return addUserVo;
    }


    //更新用户状态
    @Override
    @Transactional(rollbackFor = Exception.class)
    public UpdateUserStateVo updateUserState(Integer uId, Boolean type) {

        SpManagerEntity entity = this.spManagerDao.selectById(uId);
        if(entity == null ||entity.equals("")) {
            return null;

        }

        entity.setMgState(type? 1:0);
        this.spManagerDao.updateById(entity);

        UpdateUserStateVo userStateVo = new UpdateUserStateVo();

        ////从entity把属性赋值为userStateVo , 自动赋值
        //BeanUtils.copyProperties(entity, userStateVo);

        userStateVo.setEmail(entity.getMgEmail());
        userStateVo.setId(uId);
        userStateVo.setMobile(entity.getMgMobile());
        userStateVo.setRid(entity.getRoleId());
        userStateVo.setMg_state(entity.getMgState());
        userStateVo.setUsername(entity.getMgName());

        return userStateVo;
    }


    //通过id获取用户信息
    @Override
    public UserByIdVo getUserInfoById(Integer id) {
        SpManagerEntity entity = this.spManagerDao.selectById(id);
        if(entity.equals("") || entity == null){
            return null;
        }

        UserByIdVo userByIdVo = new UserByIdVo();
        userByIdVo.setEmail(entity.getMgEmail());
        userByIdVo.setId(entity.getMgId());
        userByIdVo.setMobile(entity.getMgMobile());
        userByIdVo.setRole_id(entity.getRoleId());
        userByIdVo.setUsername(entity.getMgName());

        return userByIdVo;
    }

    //返回是否存在查询结果
    @Override
    public int selectCount(String field, String key) {
        return this.spManagerDao.selectCount(new QueryWrapper<SpManagerEntity>().eq(field,key));
    }


    //更新用户信息
    @Override
    @Transactional(rollbackFor = Exception.class)
    public UpdateUserVo updateUserInfo(Integer id, Map<String,Object> paraMap) {


        SpManagerEntity entity = new SpManagerEntity();
        entity.setMgMobile(paraMap.get("mobile").toString());
        entity.setMgEmail(paraMap.get("email").toString());

        this.spManagerDao.update(entity,new QueryWrapper<SpManagerEntity>().eq("mg_id",id));

        UpdateUserVo userStateVo = new UpdateUserVo();

        ////从entity把属性赋值为userStateVo , 自动赋值
        //BeanUtils.copyProperties(entity, userStateVo);

        userStateVo.setEmail(entity.getMgEmail());
        userStateVo.setMobile(entity.getMgMobile());
        userStateVo.setId(id);

        return userStateVo;
    }
}
