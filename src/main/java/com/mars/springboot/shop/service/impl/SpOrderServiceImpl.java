package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpOrderDao;
import com.mars.springboot.shop.entity.SpOrderEntity;
import com.mars.springboot.shop.service.SpOrderService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpOrderServiceImpl extends ServiceImpl<SpOrderDao, SpOrderEntity> implements SpOrderService {

}
