package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpPermissionApiDao;
import com.mars.springboot.shop.entity.SpPermissionApiEntity;
import com.mars.springboot.shop.service.SpPermissionApiService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpPermissionApiServiceImpl extends ServiceImpl<SpPermissionApiDao, SpPermissionApiEntity> implements SpPermissionApiService {

}
