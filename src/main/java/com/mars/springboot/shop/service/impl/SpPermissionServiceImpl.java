package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpPermissionDao;
import com.mars.springboot.shop.entity.SpPermissionEntity;
import com.mars.springboot.shop.service.SpPermissionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpPermissionServiceImpl extends ServiceImpl<SpPermissionDao, SpPermissionEntity> implements SpPermissionService {

}
