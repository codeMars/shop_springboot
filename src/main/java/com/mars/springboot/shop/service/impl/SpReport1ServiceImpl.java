package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpReport1Dao;
import com.mars.springboot.shop.entity.SpReport1Entity;
import com.mars.springboot.shop.service.SpReport1Service;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpReport1ServiceImpl extends ServiceImpl<SpReport1Dao, SpReport1Entity> implements SpReport1Service {

}
