package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpReport2Dao;
import com.mars.springboot.shop.entity.SpReport2Entity;
import com.mars.springboot.shop.service.SpReport2Service;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpReport2ServiceImpl extends ServiceImpl<SpReport2Dao, SpReport2Entity> implements SpReport2Service {

}
