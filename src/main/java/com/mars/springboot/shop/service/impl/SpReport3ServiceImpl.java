package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpReport3Dao;
import com.mars.springboot.shop.entity.SpReport3Entity;
import com.mars.springboot.shop.service.SpReport3Service;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpReport3ServiceImpl extends ServiceImpl<SpReport3Dao, SpReport3Entity> implements SpReport3Service {

}
