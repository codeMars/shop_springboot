package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpRoleDao;
import com.mars.springboot.shop.entity.SpRoleEntity;
import com.mars.springboot.shop.service.SpRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpRoleServiceImpl extends ServiceImpl<SpRoleDao, SpRoleEntity> implements SpRoleService {

}
