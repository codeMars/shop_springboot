package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpTypeDao;
import com.mars.springboot.shop.entity.SpTypeEntity;
import com.mars.springboot.shop.service.SpTypeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 类型表 服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpTypeServiceImpl extends ServiceImpl<SpTypeDao, SpTypeEntity> implements SpTypeService {

}
