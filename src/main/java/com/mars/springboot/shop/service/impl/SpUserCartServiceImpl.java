package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpUserCartDao;
import com.mars.springboot.shop.entity.SpUserCartEntity;
import com.mars.springboot.shop.service.SpUserCartService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpUserCartServiceImpl extends ServiceImpl<SpUserCartDao, SpUserCartEntity> implements SpUserCartService {

}
