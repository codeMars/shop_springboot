package com.mars.springboot.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.springboot.shop.dao.SpUserDao;
import com.mars.springboot.shop.entity.SpUserEntity;
import com.mars.springboot.shop.service.SpUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author fengjm
 * @since 2020-02-07
 */
@Service
public class SpUserServiceImpl extends ServiceImpl<SpUserDao, SpUserEntity> implements SpUserService {



}
