package com.mars.springboot.shop.vo;


import lombok.Data;

import java.io.Serializable;


@Data
public class AddUserVo implements Serializable {

    private Integer id;
    private String username;
    private String mobile;
    private String type;
    private String email;
    private String create_time;
    private String modify_time;
    private Boolean is_delete;
    private Boolean is_active;


}
