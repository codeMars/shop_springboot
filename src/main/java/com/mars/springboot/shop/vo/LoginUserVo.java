package com.mars.springboot.shop.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoginUserVo implements Serializable {

    //姓名
    private String username;
    //用户id
    private Integer id;
    //角色id
    private Integer rid;
    private Integer mobile;
    private String email;
    private String token;

}
