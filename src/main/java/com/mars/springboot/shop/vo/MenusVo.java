package com.mars.springboot.shop.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class MenusVo implements Serializable {

    private Integer id;

    private Integer order;

    private String authName;

    private String path;

    private List<MenusVo> children;

}
