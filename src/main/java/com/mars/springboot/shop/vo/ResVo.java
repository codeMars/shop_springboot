package com.mars.springboot.shop.vo;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.mars.springboot.shop.base.StatusCode;

import java.util.HashMap;
import java.util.Map;

/**
 * 通用响应封装类
 * Created by Administrator on 2019/10/26.
 */
public class ResVo<T> {

//    {
//        "data": {
//                "id": 500,
//                "rid": 0,
//                "username": "admin",
//                "mobile": "123",
//                "email": "123@qq.com",
//                "token": "HIQ5i5L1kX9RX444uwnRGaIM"
//    },
//        "meta": {
//                "msg": "登录成功",
//                "status": 200
//    }
//    }

    private ResCode meta;
    private T data;

    public ResVo(StatusCode statusCode, T data) {
        this.meta = new ResCode(statusCode.getCode(),statusCode.getMsg());
        this.data = data;
    }

    public ResVo(Integer status,String msg, T data) {
        this.meta = new ResCode(status,msg);
        this.data = data;
    }


    public ResVo(Integer status,String msg) {
        this.meta = new ResCode(status,msg);
        this.data = null;
    }


    public ResVo(StatusCode statusCode) {
        this.meta = new ResCode(statusCode.getCode(),statusCode.getMsg());

    }

    public ResCode getMeta() {
        return meta;
    }

    public void setMeta(StatusCode statusCode) {
        this.meta = new ResCode(statusCode.getCode(),statusCode.getMsg());
    }

    public void setMeta(Integer code,String msg) {
        this.meta = new ResCode(code,msg);
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

//    public static void main(String[] args) {
//        ResVo resVo = new ResVo(StatusCode.OK,"data~~");
//        System.out.println(JSON.toJSONString(resVo));
//
//        ResVo resVo2 = new ResVo(123,"222","{dddddd;dd}~~");
//        System.out.println(JSON.toJSONString(resVo2));
//
//
//        ResVo resVo3 = new ResVo(StatusCode.OK);
//        System.out.println(JSON.toJSONString(resVo3));
//
//        ResVo resVo4 = new ResVo(123,"44444");
//        System.out.println(JSON.toJSONString(resVo4));
//    }

}


class ResCode{

    private Integer status;
    private String msg;

    public ResCode(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}

    //    private Integer code;
//    private String msg;
//
//    private T data;
//
//    public T getMeta() {
//        return meta;
//    }
//
//    public void setMeta(T meta) {
//        this.meta = meta;
//    }
//
//    private T meta;
//
//    public ResVo(T statusCode, T data){
////        this.code = statusCode.getCode();
////        this.msg = statusCode.getMsg();
//        this.meta = statusCode;
//        this.data = data;
//
//    }
//
//    public ResVo(StatusCode statusCode){
//        this.code = statusCode.getCode();
//        this.msg = statusCode.getMsg();
//
//    }
//
//    public ResVo(Integer code, String msg, T data) {
//        this.code = code;
//        this.msg = msg;
//        this.data = data;
//    }
//
//    public ResVo(Integer code, String msg) {
//        this.code = code;
//        this.msg = msg;
//    }
//
//
//    public Integer getCode() {
//        return code;
//    }
//
//    public void setCode(Integer code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public T getData() {
//        return data;
//    }
//
//    public void setData(T data) {
//        this.data = data;
//    }
//
//    public static void main(String[] args) {
//        ResVo resVo = new ResVo(StatusCode.Success);
//        HashMap<String,Object> re = Maps.newHashMap();
//        re.put("msg",resVo.getMsg());
//        re.put("status",resVo.code);
//        resVo.setMeta(re);
//        resVo.setData(resVo.getCode());
//
//        System.out.println(JSON.toJSONString(resVo));
//    }

