package com.mars.springboot.shop.vo;

import com.mars.springboot.shop.base.StatusCode;

/**
 * 通用响应封装类
 * Created by Administrator on 2019/10/26.
 */
public class ResultVo<T> {

//    {
//        "data": {
//                "id": 500,
//                "rid": 0,
//                "username": "admin",
//                "mobile": "123",
//                "email": "123@qq.com",
//                "token": "HIQ5i5L1kX9RX444uwnRGaIM"
//    },
//        "meta": {
//                "msg": "登录成功",
//                "status": 200
//    }
//    }


    private Integer code;
    private String msg;

    private T data;

    public ResultVo(StatusCode statusCode, T data){
        this.code = statusCode.getCode();
        this.msg = statusCode.getMsg();
        this.data = data;

    }

    public ResultVo(StatusCode statusCode){
        this.code = statusCode.getCode();
        this.msg = statusCode.getMsg();

    }

    public ResultVo(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResultVo(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
