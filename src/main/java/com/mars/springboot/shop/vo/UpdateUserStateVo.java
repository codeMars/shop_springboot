package com.mars.springboot.shop.vo;


import lombok.Data;

import java.io.Serializable;


@Data
public class UpdateUserStateVo implements Serializable {

    private Integer id;
    private Integer rid;
    private String username;
    private String mobile;
    private Integer mg_state;
    private String email;



}
