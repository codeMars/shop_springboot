package com.mars.springboot.shop.vo;


import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class UpdateUserVo implements Serializable {


    private Integer id;
    private String mobile;
    private String email;

}
