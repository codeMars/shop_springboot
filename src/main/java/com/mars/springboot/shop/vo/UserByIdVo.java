package com.mars.springboot.shop.vo;


import lombok.Data;

import java.io.Serializable;

@Data
public class UserByIdVo implements Serializable {

    private Integer id;
    private Integer role_id;
    private String mobile;
    private String email;
    private String username;

}
