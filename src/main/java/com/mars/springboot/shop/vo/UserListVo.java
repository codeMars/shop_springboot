package com.mars.springboot.shop.vo;

import com.mars.springboot.shop.entity.UserEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UserListVo implements Serializable {


    private Long total;

    private Long pagenum;

    private List<UserEntity> users;


}
