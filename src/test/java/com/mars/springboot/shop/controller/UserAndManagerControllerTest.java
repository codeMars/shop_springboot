package com.mars.springboot.shop.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.swing.text.AbstractDocument;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.*;

public class UserAndManagerControllerTest {

    private final String baseUrl = "http://localhost:8881/shop";
    private final String logiUrl = "/api/private/v1/login";

    @BeforeMethod
    public void setUp() throws IOException {
        String url = baseUrl+logiUrl;
        CloseableHttpClient httpClient = HttpClients.createDefault();
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(2000)
                .setSocketTimeout(10000).build();

        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
        httpPost.setConfig(requestConfig);

        //添加参数
        JSONObject param = new JSONObject();
        param.put("username","fengjm");
        param.put("password","123456");

        String parameter = JSON.toJSONString(param);

        //将参数信息添加到方法中
        StringEntity entity = new StringEntity(parameter, "utf-8");
        httpPost.setEntity(entity);

        //cookies
        CookieStore store =new BasicCookieStore();

        //声明一个client对象，用来进行方法的执行,并设置cookies信息
        CloseableHttpClient httpclient = HttpClients.custom().setDefaultCookieStore(store).build();

        //执行post的方法并得到响应结果
        CloseableHttpResponse response = httpclient.execute(httpPost);

        //就是判断返回结果是否符合预期
        int statusCode = response.getStatusLine().getStatusCode();
        System.out.println("statusCode = "+ statusCode);

        String result=EntityUtils.toString(response.getEntity(),"utf-8");;

        if (statusCode==200){
            System.out.println(result);
        } else {
            System.out.println("login接口失败");
        }

        //将返回的响应结果字符串转化为json对象
        JSONObject resultjson = JSONObject.parseObject(result);

        //获取到结果值
        String resultData = resultjson.get("data").toString();
        System.out.println("data: "+resultData);


        //读取cookie信息
        List<Cookie> cookielist = store.getCookies();
        System.out.println("cookielist is : "+cookielist);
        for(Cookie cookie: cookielist){
            String name=cookie.getName();
            String value=cookie.getValue();
            System.out.println("cookie name =" + name);
            System.out.println("Cookie value=" + value);
        }

        //jsonstring转换为map
        //第一种方式
        Map maps = (Map)JSON.parse(result);
        System.out.println("这个是用JSON类来解析JSON字符串!!!");
        for (Object map : maps.entrySet()){
            System.out.println(((Map.Entry)map).getKey()+"     " + ((Map.Entry)map).getValue());
        }

        //第二种方式 --推荐
        Map mapTypes = JSON.parseObject(result);
        System.out.println("这个是用JSON类的parseObject来解析JSON字符串!!!");
        for (Object obj : mapTypes.keySet()){
            System.out.println("key为："+obj+"值为："+mapTypes.get(obj));
        }
        //第三种方式
        Map mapType = JSON.parseObject(result,Map.class);
        System.out.println("这个是用JSON类,指定解析类型，来解析JSON字符串!!!");
        for (Object obj : mapType.keySet()){
            System.out.println("key为："+obj+"值为："+mapType.get(obj));
        }
        //第四种方式
        /**
         * JSONObject是Map接口的一个实现类
         */
        Map json = (Map) JSONObject.parse(result);
        System.out.println("这个是用JSONObject类的parse方法来解析JSON字符串!!!");
        for (Object map : json.entrySet()){
            System.out.println(((Map.Entry)map).getKey()+"  "+((Map.Entry)map).getValue());
        }
        //第五种方式
        /**
         * JSONObject是Map接口的一个实现类
         */
        JSONObject jsonObject = JSONObject.parseObject(result);
        System.out.println("这个是用JSONObject的parseObject方法来解析JSON字符串!!!");
        for (Object map : json.entrySet()){
            System.out.println(((Map.Entry)map).getKey()+"  "+((Map.Entry)map).getValue());
        }
        //第六种方式
        /**
         * JSONObject是Map接口的一个实现类
         */
        Map mapObj = JSONObject.parseObject(result,Map.class);
        System.out.println("这个是用JSONObject的parseObject方法并执行返回类型来解析JSON字符串!!!");
        for (Object map: json.entrySet()){
            System.out.println(((Map.Entry)map).getKey()+"  "+((Map.Entry)map).getValue());
        }

    }


    @Test
    public void testGetUsers() throws IOException {
        String usersUri = "http://localhost:8881/shop/api/private/v1/users?query=&pagenum=1&pagesize=1";

        CloseableHttpClient httpClient = HttpClients.createDefault();
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(2000)
                .setSocketTimeout(10000).build();

        HttpGet httpGet = new HttpGet(usersUri);
        httpGet.setConfig(requestConfig);

        CloseableHttpResponse response = null;
        String result = null;

        response = httpClient.execute(httpGet);
//        try {
//            result = EntityUtils.toString(response.getEntity(), "utf-8");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        System.out.println(result);

        int status = response.getStatusLine().getStatusCode();//获取返回状态值
        if (status == HttpStatus.SC_OK) {//请求成功
            HttpEntity httpEntity = response.getEntity();
            if (httpEntity != null) {
                result = EntityUtils.toString(httpEntity, "UTF-8");
                EntityUtils.consume(httpEntity);//关闭资源
                JSONObject jsonResult = JSONObject.parseObject(result);
//                JSONArray jsonArray = jsonResult.getJSONArray("data");
//                for (int i = 0; i < jsonArray.size(); i++) {//只取一个值返回
//                    result = jsonArray.getJSONObject(i).getString("对应key");
//                }
                System.out.println("result:  "+result);
               // return result;
            }
        }
      //  return result;
    }


    //get请求
    private String testGet() {
        //创建 CloseableHttpClient
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String result = null;
        try {
            URIBuilder uri = new URIBuilder("请求路径");
            //get请求带参数
            List<NameValuePair> list = new LinkedList<>();
            BasicNameValuePair param1 = new BasicNameValuePair("key1", "value1");
            BasicNameValuePair param2 = new BasicNameValuePair("key2", "value2");
            list.add(param1);
            list.add(param2);
            uri.setParameters(list);
            HttpGet httpGet = new HttpGet(uri.build());
            //设置请求状态参数
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectionRequestTimeout(3000)
                    .setSocketTimeout(3000)
                    .setConnectTimeout(3000).build();
            httpGet.setConfig(requestConfig);
            response = httpClient.execute(httpGet);
            int status = response.getStatusLine().getStatusCode();//获取返回状态值
            if (status == HttpStatus.SC_OK) {//请求成功
                HttpEntity httpEntity = response.getEntity();
                if (httpEntity != null) {
                    result = EntityUtils.toString(httpEntity, "UTF-8");
                    EntityUtils.consume(httpEntity);//关闭资源
                    JSONObject jsonResult = JSONObject.parseObject(result);
                    // JSONObject如下格式
                    /*{
                        "data ": [
                                  {
                                      "name": "*",
                                      "value": "*",
                                  }
                              ],
                              "success ": true
                      }*/
                    JSONArray jsonArray = jsonResult.getJSONArray("data");
                    for (int i = 0; i < jsonArray.size(); i++) {//只取一个值返回
                        result = jsonArray.getJSONObject(i).getString("对应key");
                    }
                    return result;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }


    //post请求
    private void testPost() {
        HttpPost httpPost = new HttpPost("请求路径");
        CloseableHttpClient client = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        List params = new ArrayList();
        params.add(new BasicNameValuePair("key1", "value1"));
        params.add(new BasicNameValuePair("key2", "value2"));
        try {
            HttpEntity httpEntity = new UrlEncodedFormEntity(params, "UTF-8");
            httpPost.setEntity(httpEntity);
            response = client.execute(httpPost);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (client != null) {
                try {
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}